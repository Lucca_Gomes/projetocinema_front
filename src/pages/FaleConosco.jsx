import React, { useState } from 'react';
import styles from "../assets/css/faleconosco.module.css";
import { Modal } from '../components/ModalFaleConosco'; 

export function FaleConosco(params) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [formData, setFormData] = useState({
        nome: "",
        assunto: "",
        descricao: ""
    });
    
    const openModal = () => {
        if (isFormValid()) {
            setIsModalOpen(true);
        } else {
            const missingFields = [];
            if (formData.nome.trim() === "") {
                missingFields.push("Nome Completo");
            }
            if (formData.assunto.trim() === "") {
                missingFields.push("Assunto");
            }
            if (formData.descricao.trim() === "") {
                missingFields.push("Descrição Detalhada");
            }
    
            const missingFieldsText = missingFields.join(", ");
            alert(`Por favor, preencha os seguintes campos obrigatórios: ${missingFieldsText}`);
        }
    };
    
    const closeModal = () => {
        setIsModalOpen(false);
    };

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData((prevFormData) => ({
            ...prevFormData,
            [name]: value
        }));
    };

    const isFormValid = () => {
        const { nome, assunto, descricao } = formData;
        return nome.trim() !== "" && assunto.trim() !== "" && descricao.trim() !== "";
    };

    return (
        <main>
            <form className={styles.form_contact}>
                <section className={styles.form_text}>
                    <h1>Contato</h1>
                    <h3>Encontrou algum problema? <br /> Envie uma mensagem!</h3>
                </section>
                <section className={styles.form_input}>
                    <input type="text" name="nome" placeholder="Nome Completo" required onChange={handleInputChange} />
                    <input type="text" name="assunto" placeholder="Assunto" required onChange={handleInputChange} />
                    <textarea name="descricao" rows="10" placeholder="Descrição Detalhada" required onChange={handleInputChange}></textarea>
                    <button type="button" onClick={openModal}>Enviar</button>
                </section>
            </form>

            {isModalOpen && (
                <Modal isOpen={isModalOpen} onClose={closeModal}>
                    {/* Conteúdo do modal */}
                </Modal>
            )}
        </main>        
    );
}
