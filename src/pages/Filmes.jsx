import styles from "../assets/css/filmes.module.css";
import { Movies } from "../components/Movies";
import { useState } from "react";
import { useEffect } from "react";

export function Filmes(params) {
    const [filmes, setFilmes] = useState([]);

    useEffect(() => {
        const url = "http://localhost:3333/filmes";
        const config = {
            method: "GET",
        };
        
        fetch(url, config)
            .then((response) => response.json())
            .then((dados) => {
                setFilmes(dados);
            })
            .catch((error) => {
                console.error('Error fetching movies:', error);
            });
    },[])
    
    return (
        <div>
            <div className={styles.search}>
                <div>
                    <section className={styles.search_input}>
                            <input type="text" placeholder="Pesquisar filmes" />
                            <img src="src/assets/img/lupa.svg" alt="" className={styles.lupa}/>
                    </section>
                    <section className={styles.filter}>
                                <div className={styles.option_filter}>
                                    <select name="" id="">
                                        <option  value="">Gênero</option>
                                        <option value="acao">Ação</option>
                                        <option value="aventura">Aventura</option>
                                        <option value="terror">Terror</option>
                                        <option value="drama">Drama</option>
                                        <option value="romance">Romance</option>
                                    </select>
                                </div>
                                <div className={styles.option_filter}>
                                    <select name="" id="" >
                                        <option value="">Classificação</option>
                                        <option value="livre">Livre</option>
                                        <option value="10">10</option>
                                        <option value="12">12</option>
                                        <option value="16">16</option>
                                        <option value="18">18</option>
                                    </select>
                                </div>
                    </section>
                </div>
            </div>
            <div className={styles.main_title}>
                <h1>Filmes</h1>
            </div>
            <div className={styles.cards}>
                <Movies filmes={filmes.slice(0,3)}/>
                <Movies filmes={filmes.slice(3,6)}/>
                <Movies filmes={filmes.slice(6,9)}/>
            </div>
            <div className={styles.cards}>
                {/* <Movies filmes={filmes}/> */}
                {/* <Movies/>
                <Movies/> */}
            </div>
            <div className={styles.pagination}>
                <img src="src/assets/img/anterior.svg" alt="" />
                <img src="src/assets/img/prox.svg" alt="" />
            </div>
        </div>
    )
}