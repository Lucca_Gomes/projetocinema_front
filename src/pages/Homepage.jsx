import { useEffect, useState} from "react";
import styles from "../assets/css/homepage.module.css";
import { Carrosel } from "../components/Carrosel";
import { NavLink } from "react-router-dom";


export function HomePage() {

    const [ filmes, setFilmes] = useState([]);

        useEffect (() => {
            const url = "http://localhost:3333/filmes"
            const config = {
                method: "GET"

            }
            fetch(url, config)
            .then((response) => {
                response.json() .then ((dados) => {
                    console.log(dados)
                    setFilmes(dados)
                })
            }

            )
        }, [])

    const array = [1,2,3,4,5];

    return (
        <>
        <section className={styles.introduction_background}>
            <div className={styles.introduction}>
                <div>
                   <h1>Transformando Filmes em Experiências Personalizadas</h1>
                   <h3>Reserve Seu Assento e Viva a Magia do Cinema!</h3>
                </div>
            </div>
        </section>
            <Carrosel/>
            <main className={styles.main_poster}>
                <div>
                    <section className={styles.poster_title}>
                        <h4>Em Cartaz</h4>
                    </section>
                        <section className={styles.poster_movies}>
                            { filmes.length >= 5 &&
                                array.map ((valor, index) => {
                                    const filmeIndex = Math.floor(Math.random() * (filmes.length-1)) 
                                    const filmeElemento = filmes[filmeIndex]
                                    return <div className={styles.poster_filme}>
                                    <div><img src={filmeElemento.imgURL} alt="" /></div>
                                    <h5 >{filmeElemento.titulo}</h5>
                                    <NavLink to={`/mostrarsessoes/${filmeElemento.id}`}>Sessões Disponíveis</NavLink>
                                </div>
                                })
                            }
                        </section>
                        <section className={styles.button}>
                            <NavLink to="/filmes">Ver mais</NavLink>
                        </section> 
                </div>            
            </main>        

        </>
    )
}