import React, { useState, useEffect } from 'react';
import styles from '../assets/css/mostrar_sessoes.module.css';
import { Sessoes } from '../components/Sessoes';
import { useParams } from 'react-router-dom';

export function MostrarSessoes() {
    const { filmeId } = useParams();
    const [filme, setFilme] = useState(null);

    useEffect(() => {
        fetch(`http://localhost:3333/filmes/${filmeId}`)
            .then(response => response.json())
            .then(dados => {
                setFilme(dados);
            })
            .catch(error => {
                console.error('Error fetching movie details:', error);
            });
    }, [filmeId]);

    if (!filme) {
        return <p>Loading...</p>;
    }

    return (
        <div>
            <div className={styles.banner}>
                <div>
                    <div className={styles.card}>
                        <img src={filme.imgURL} alt={filme.titulo} />
                    </div>
                    <div className={styles.banner_container}>
                        <div className={styles.card_title}>
                            <h1>{filme.titulo}</h1>
                            <span>{filme.classificacao}</span>
                        </div>
                        <div className={styles.card_description}>
                            <span>{filme.genero}</span>
                            <p>{filme.sinopse}</p>
                        </div>
                        <div className={styles.filter}>
                        <div className={styles.filter_expansive}>
                                <select name="" id="">
                                    <option value="">Cidade</option>
                                    <option value="niteroi">Niteroi, RJ</option>
                                    <option value="rj">Rio de Janeiro, RJ</option>
                                    <option value="sg">São Gonçalo, RJ</option>
                                </select>
                            </div>
                            <div className={styles.filter_expansive}>
                                <select name="" id="">
                                    <option value="">Bairro</option>
                                    <option value="icarai">Icaraí</option>
                                    <option value="copacabana">Copacabana</option>
                                    <option value="centro">Centro</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Sessoes />
        </div>
    );
}
