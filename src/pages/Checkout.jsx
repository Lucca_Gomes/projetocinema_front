import { useEffect, useState } from 'react'
import styles from '../assets/css/checkout.module.css'
import Modal from '../components/Modal';
import Modal1 from '../components/Modal1';
// import disponivel from '../assets/img/cadeira_disponivel.svg';
// import selecionada from '../assets/img/cadeira_selecionada.svg';

export function Checkout (){

    const [ openModal, setOpenModal] = useState (false);
    const [ openModal1, setOpenModal1] = useState (false);


    const [cadeirasSelecionadas, setCadeirasSelecionadas] = useState([])

    const disponivel = "src/assets/img/cadeira_disponivel.svg";
    const selecionada = "src/assets/img/cadeira_selecionada.svg";

    var [lista_cadeiras, setCadeiras] = useState([]);
    
    // useEffect(()=> {
    //     var lista_cadeira = []

    //     for(let a = 65; a <= 74; a++){
    //         let id1=0;
    //         for(let b=1; b<19; b++){
    //             let objeto = {
    //                 id: id1,
    //                 fileira: String.fromCharCode(a),
    //                 assento: b,
    //                 disponibilidade: true,
    //                 selecionado: false,
    //                 img: "src/assets/img/cadeira_disponivel.svg"
    //             }
    //             lista_cadeira.push(objeto);
    //             id1++;
    //         }
    //     }
    //     setCadeiras(lista_cadeira);
    // }, [])

    var [lista_cadeiras, setCadeiras] = useState ([
        {
            fileira: "A",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 2,
            situacao: disponivel 
        },{
            fileira: "A",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "A",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "B",
            assento: 18,
            situacao: disponivel    
        },{
            fileira: "C",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "C",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "D",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "E",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "F",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "G",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "H",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "I",
            assento: 18,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 1,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 2,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 3,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 4,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 5,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 6,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 7,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 8,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 9,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 10,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 11,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 12,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 13,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 14,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 15,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 16,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 17,
            situacao: disponivel
        },{
            fileira: "J",
            assento: 18,
            situacao: disponivel
        }]);


    
        function chair_selector(value){
            if(lista_cadeiras[value].situacao ===  "src/assets/img/cadeira_disponivel.svg"){
                setCadeiras ([...lista_cadeiras, {...lista_cadeiras[value]}, lista_cadeiras[value].situacao = selecionada]);
                setCadeiras ([...lista_cadeiras, {...lista_cadeiras[value] = false}]);
            } else {
                setCadeiras ([...lista_cadeiras, {...lista_cadeiras[value]}, lista_cadeiras[value].situacao = disponivel]);
                setCadeiras ([...lista_cadeiras, {...lista_cadeiras[value] = true}]);
            }
        }
    
        const onNextModal = () =>{
            setOpenModal(false);
            setOpenModal1(true);
        };
    

    return (
        <div className={styles.checkout}>
            {console.log(lista_cadeiras)}
            <aside>
                <div>
                    <div>
                        <img src="src/assets/img/Capa-Exemplo.svg" />
                        <div>
                            <p>Besouro Azul</p>
                            <div>
                                <img src="" />
                                <img src="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <p><img src="src/assets/img/Checkout-Cadeira.svg" />          ASSENTOS ESCOLHIDOS</p>
                    <div id="div_quadro"> 
                        {
                            <form className={styles.form_assentos}>
                                {lista_cadeiras.map( objeto => {
                                    if (objeto.situacao === selecionada){
                                        return (
                                            <div>
                                                <p>{objeto.fileira}{objeto.assento}</p>
                                                <label>NOME</label>
                                                <textarea></textarea>
                                                <label>CPF</label>
                                                <textarea></textarea>
                                            </div>   
                                        )
                                    }})
                                }
                            </form> 
                        }
                    </div> 

                    <button onClick={() => setOpenModal(true)} >CONFIRMAR</button>
                </div>              
            </aside>
            <aside>
                <img src="src/assets/img/Tela.svg" />               
                <div>
                    <img src="src/assets/img/A-J.svg" />
                    <div>
                        {/* <div className={styles.div_lista}>                
                            {lista_cadeiras.map(cadeira =>(
                                <button onClick={()=>chair_selector( cadeira.id)}><img src= {cadeira.img} />{cadeira.fileira}{cadeira.assento}</button>                               
                            ))}
                        </div> */}
                        <div>
                            <button onClick={()=> chair_selector(0)}><img src={lista_cadeiras[0].situacao} /></button>
                            <button onClick={()=> chair_selector(1)}><img src={lista_cadeiras[1].situacao} /></button>
                            <button onClick={()=> chair_selector(2)}><img src={lista_cadeiras[2].situacao} /></button>
                            <button onClick={()=> chair_selector(3)}><img src={lista_cadeiras[3].situacao} /></button>
                            <button onClick={()=> chair_selector(4)}><img src={lista_cadeiras[4].situacao} /></button>
                            <button onClick={()=> chair_selector(5)}><img src={lista_cadeiras[5].situacao} /></button>
                            <button onClick={()=> chair_selector(6)}><img src={lista_cadeiras[6].situacao} /></button>
                            <button onClick={()=> chair_selector(7)}><img src={lista_cadeiras[7].situacao} /></button>
                            <button onClick={()=> chair_selector(8)}><img src={lista_cadeiras[8].situacao} /></button>
                            <button onClick={()=> chair_selector(9)}><img src={lista_cadeiras[9].situacao} /></button>
                            <button onClick={()=> chair_selector(10)}><img src={lista_cadeiras[10].situacao} /></button>
                            <button onClick={()=> chair_selector(11)}><img src={lista_cadeiras[11].situacao} /></button>
                            <button onClick={()=> chair_selector(12)}><img src={lista_cadeiras[12].situacao} /></button>
                            <button onClick={()=> chair_selector(13)}><img src={lista_cadeiras[13].situacao} /></button>
                            <button onClick={()=> chair_selector(14)}><img src={lista_cadeiras[14].situacao} /></button>
                            <button onClick={()=> chair_selector(15)}><img src={lista_cadeiras[15].situacao} /></button>
                            <button onClick={()=> chair_selector(16)}><img src={lista_cadeiras[16].situacao} /></button>
                            <button onClick={()=> chair_selector(17)}><img src={lista_cadeiras[17].situacao} /></button>                          
                        </div>
                        <div>
                            <button onClick={()=> chair_selector(18)}><img src={lista_cadeiras[18].situacao} /></button>
                            <button onClick={()=> chair_selector(19)}><img src={lista_cadeiras[19].situacao} /></button>
                            <button onClick={()=> chair_selector(20)}><img src={lista_cadeiras[20].situacao} /></button>
                            <button onClick={()=> chair_selector(21)}><img src={lista_cadeiras[21].situacao} /></button>
                            <button onClick={()=> chair_selector(22)}><img src={lista_cadeiras[22].situacao} /></button>
                            <button onClick={()=> chair_selector(23)}><img src={lista_cadeiras[23].situacao} /></button>
                            <button onClick={()=> chair_selector(24)}><img src={lista_cadeiras[24].situacao} /></button>
                            <button onClick={()=> chair_selector(25)}><img src={lista_cadeiras[25].situacao} /></button>
                            <button onClick={()=> chair_selector(26)}><img src={lista_cadeiras[26].situacao} /></button>
                            <button onClick={()=> chair_selector(27)}><img src={lista_cadeiras[27].situacao} /></button>
                            <button onClick={()=> chair_selector(28)}><img src={lista_cadeiras[28].situacao} /></button>
                            <button onClick={()=> chair_selector(29)}><img src={lista_cadeiras[29].situacao} /></button>
                            <button onClick={()=> chair_selector(30)}><img src={lista_cadeiras[30].situacao} /></button>
                            <button onClick={()=> chair_selector(31)}><img src={lista_cadeiras[31].situacao} /></button>
                            <button onClick={()=> chair_selector(32)}><img src={lista_cadeiras[32].situacao} /></button>
                            <button onClick={()=> chair_selector(33)}><img src={lista_cadeiras[33].situacao} /></button>
                            <button onClick={()=> chair_selector(34)}><img src={lista_cadeiras[34].situacao} /></button> 
                            <button onClick={()=> chair_selector(35)}><img src={lista_cadeiras[35].situacao} /></button>                         
                        </div>
                        <div>
                            <button onClick={()=> chair_selector(36)}><img src={lista_cadeiras[36].situacao} /></button>
                            <button onClick={()=> chair_selector(37)}><img src={lista_cadeiras[37].situacao} /></button>
                            <button onClick={()=> chair_selector(38)}><img src={lista_cadeiras[38].situacao} /></button>
                            <button onClick={()=> chair_selector(39)}><img src={lista_cadeiras[39].situacao} /></button>
                            <button onClick={()=> chair_selector(40)}><img src={lista_cadeiras[40].situacao} /></button>
                            <button onClick={()=> chair_selector(41)}><img src={lista_cadeiras[41].situacao} /></button>
                            <button onClick={()=> chair_selector(42)}><img src={lista_cadeiras[42].situacao} /></button>
                            <button onClick={()=> chair_selector(43)}><img src={lista_cadeiras[43].situacao} /></button>
                            <button onClick={()=> chair_selector(44)}><img src={lista_cadeiras[44].situacao} /></button>
                            <button onClick={()=> chair_selector(45)}><img src={lista_cadeiras[45].situacao} /></button>
                            <button onClick={()=> chair_selector(46)}><img src={lista_cadeiras[46].situacao} /></button>
                            <button onClick={()=> chair_selector(47)}><img src={lista_cadeiras[47].situacao} /></button>
                            <button onClick={()=> chair_selector(48)}><img src={lista_cadeiras[48].situacao} /></button>
                            <button onClick={()=> chair_selector(49)}><img src={lista_cadeiras[49].situacao} /></button>
                            <button onClick={()=> chair_selector(50)}><img src={lista_cadeiras[50].situacao} /></button>
                            <button onClick={()=> chair_selector(51)}><img src={lista_cadeiras[51].situacao} /></button>
                            <button onClick={()=> chair_selector(52)}><img src={lista_cadeiras[52].situacao} /></button>
                            <button onClick={()=> chair_selector(53)}><img src={lista_cadeiras[53].situacao} /></button>                      
                        </div>
                        <div>               
                            <button onClick={()=> chair_selector(54)}><img src={lista_cadeiras[54].situacao} /></button>
                            <button onClick={()=> chair_selector(55)}><img src={lista_cadeiras[55].situacao} /></button>
                            <button onClick={()=> chair_selector(56)}><img src={lista_cadeiras[56].situacao} /></button>
                            <button onClick={()=> chair_selector(57)}><img src={lista_cadeiras[57].situacao} /></button>
                            <button onClick={()=> chair_selector(58)}><img src={lista_cadeiras[58].situacao} /></button>
                            <button onClick={()=> chair_selector(59)}><img src={lista_cadeiras[59].situacao} /></button>
                            <button onClick={()=> chair_selector(60)}><img src={lista_cadeiras[60].situacao} /></button>
                            <button onClick={()=> chair_selector(61)}><img src={lista_cadeiras[61].situacao} /></button>
                            <button onClick={()=> chair_selector(62)}><img src={lista_cadeiras[62].situacao} /></button>
                            <button onClick={()=> chair_selector(63)}><img src={lista_cadeiras[63].situacao} /></button>
                            <button onClick={()=> chair_selector(64)}><img src={lista_cadeiras[64].situacao} /></button>
                            <button onClick={()=> chair_selector(65)}><img src={lista_cadeiras[65].situacao} /></button>
                            <button onClick={()=> chair_selector(66)}><img src={lista_cadeiras[66].situacao} /></button>
                            <button onClick={()=> chair_selector(67)}><img src={lista_cadeiras[67].situacao} /></button>
                            <button onClick={()=> chair_selector(68)}><img src={lista_cadeiras[68].situacao} /></button>
                            <button onClick={()=> chair_selector(69)}><img src={lista_cadeiras[69].situacao} /></button>
                            <button onClick={()=> chair_selector(70)}><img src={lista_cadeiras[70].situacao} /></button>   
                            <button onClick={()=> chair_selector(71)}><img src={lista_cadeiras[71].situacao} /></button>                       
                        </div>
                        <div>                          
                            <button onClick={()=> chair_selector(72)}><img src={lista_cadeiras[72].situacao} /></button>
                            <button onClick={()=> chair_selector(73)}><img src={lista_cadeiras[73].situacao} /></button>
                            <button onClick={()=> chair_selector(74)}><img src={lista_cadeiras[74].situacao} /></button>
                            <button onClick={()=> chair_selector(75)}><img src={lista_cadeiras[75].situacao} /></button>
                            <button onClick={()=> chair_selector(76)}><img src={lista_cadeiras[76].situacao} /></button>
                            <button onClick={()=> chair_selector(77)}><img src={lista_cadeiras[77].situacao} /></button>
                            <button onClick={()=> chair_selector(78)}><img src={lista_cadeiras[78].situacao} /></button>
                            <button onClick={()=> chair_selector(79)}><img src={lista_cadeiras[79].situacao} /></button>
                            <button onClick={()=> chair_selector(80)}><img src={lista_cadeiras[80].situacao} /></button>
                            <button onClick={()=> chair_selector(81)}><img src={lista_cadeiras[81].situacao} /></button>
                            <button onClick={()=> chair_selector(82)}><img src={lista_cadeiras[82].situacao} /></button>
                            <button onClick={()=> chair_selector(83)}><img src={lista_cadeiras[83].situacao} /></button>
                            <button onClick={()=> chair_selector(84)}><img src={lista_cadeiras[84].situacao} /></button>
                            <button onClick={()=> chair_selector(85)}><img src={lista_cadeiras[85].situacao} /></button>
                            <button onClick={()=> chair_selector(86)}><img src={lista_cadeiras[86].situacao} /></button>
                            <button onClick={()=> chair_selector(87)}><img src={lista_cadeiras[87].situacao} /></button>
                            <button onClick={()=> chair_selector(88)}><img src={lista_cadeiras[88].situacao} /></button>   
                            <button onClick={()=> chair_selector(89)}><img src={lista_cadeiras[89].situacao} /></button>                                                   
                        </div>
                        <div>                            
                            <button onClick={()=> chair_selector(90)}><img src={lista_cadeiras[90].situacao} /></button>
                            <button onClick={()=> chair_selector(91)}><img src={lista_cadeiras[91].situacao} /></button>
                            <button onClick={()=> chair_selector(92)}><img src={lista_cadeiras[92].situacao} /></button>
                            <button onClick={()=> chair_selector(93)}><img src={lista_cadeiras[93].situacao} /></button>
                            <button onClick={()=> chair_selector(94)}><img src={lista_cadeiras[94].situacao} /></button>
                            <button onClick={()=> chair_selector(95)}><img src={lista_cadeiras[95].situacao} /></button>
                            <button onClick={()=> chair_selector(96)}><img src={lista_cadeiras[96].situacao} /></button>
                            <button onClick={()=> chair_selector(97)}><img src={lista_cadeiras[97].situacao} /></button>
                            <button onClick={()=> chair_selector(98)}><img src={lista_cadeiras[98].situacao} /></button>
                            <button onClick={()=> chair_selector(99)}><img src={lista_cadeiras[99].situacao} /></button>
                            <button onClick={()=> chair_selector(100)}><img src={lista_cadeiras[100].situacao} /></button>
                            <button onClick={()=> chair_selector(101)}><img src={lista_cadeiras[101].situacao} /></button>
                            <button onClick={()=> chair_selector(102)}><img src={lista_cadeiras[102].situacao} /></button>
                            <button onClick={()=> chair_selector(103)}><img src={lista_cadeiras[103].situacao} /></button>
                            <button onClick={()=> chair_selector(104)}><img src={lista_cadeiras[104].situacao} /></button>
                            <button onClick={()=> chair_selector(105)}><img src={lista_cadeiras[105].situacao} /></button>
                            <button onClick={()=> chair_selector(106)}><img src={lista_cadeiras[106].situacao} /></button> 
                            <button onClick={()=> chair_selector(107)}><img src={lista_cadeiras[107].situacao} /></button>                                                     
                        </div>
                        <div>                          
                            <button onClick={()=> chair_selector(108)}><img src={lista_cadeiras[108].situacao} /></button>
                            <button onClick={()=> chair_selector(109)}><img src={lista_cadeiras[109].situacao} /></button>
                            <button onClick={()=> chair_selector(110)}><img src={lista_cadeiras[110].situacao} /></button>
                            <button onClick={()=> chair_selector(111)}><img src={lista_cadeiras[111].situacao} /></button>
                            <button onClick={()=> chair_selector(112)}><img src={lista_cadeiras[112].situacao} /></button>
                            <button onClick={()=> chair_selector(113)}><img src={lista_cadeiras[113].situacao} /></button>
                            <button onClick={()=> chair_selector(114)}><img src={lista_cadeiras[114].situacao} /></button>
                            <button onClick={()=> chair_selector(115)}><img src={lista_cadeiras[115].situacao} /></button>
                            <button onClick={()=> chair_selector(116)}><img src={lista_cadeiras[116].situacao} /></button>
                            <button onClick={()=> chair_selector(117)}><img src={lista_cadeiras[117].situacao} /></button>
                            <button onClick={()=> chair_selector(118)}><img src={lista_cadeiras[118].situacao} /></button>
                            <button onClick={()=> chair_selector(119)}><img src={lista_cadeiras[119].situacao} /></button>
                            <button onClick={()=> chair_selector(120)}><img src={lista_cadeiras[120].situacao} /></button>
                            <button onClick={()=> chair_selector(121)}><img src={lista_cadeiras[121].situacao} /></button>
                            <button onClick={()=> chair_selector(122)}><img src={lista_cadeiras[122].situacao} /></button>
                            <button onClick={()=> chair_selector(123)}><img src={lista_cadeiras[123].situacao} /></button>
                            <button onClick={()=> chair_selector(124)}><img src={lista_cadeiras[124].situacao} /></button>     
                            <button onClick={()=> chair_selector(125)}><img src={lista_cadeiras[125].situacao} /></button>                                                 
                        </div>
                        <div>                           
                            <button onClick={()=> chair_selector(126)}><img src={lista_cadeiras[126].situacao} /></button>
                            <button onClick={()=> chair_selector(127)}><img src={lista_cadeiras[127].situacao} /></button>
                            <button onClick={()=> chair_selector(128)}><img src={lista_cadeiras[128].situacao} /></button>
                            <button onClick={()=> chair_selector(129)}><img src={lista_cadeiras[129].situacao} /></button>
                            <button onClick={()=> chair_selector(130)}><img src={lista_cadeiras[130].situacao} /></button>
                            <button onClick={()=> chair_selector(131)}><img src={lista_cadeiras[131].situacao} /></button>
                            <button onClick={()=> chair_selector(132)}><img src={lista_cadeiras[132].situacao} /></button>
                            <button onClick={()=> chair_selector(133)}><img src={lista_cadeiras[133].situacao} /></button>
                            <button onClick={()=> chair_selector(134)}><img src={lista_cadeiras[134].situacao} /></button>
                            <button onClick={()=> chair_selector(135)}><img src={lista_cadeiras[135].situacao} /></button>
                            <button onClick={()=> chair_selector(136)}><img src={lista_cadeiras[136].situacao} /></button>
                            <button onClick={()=> chair_selector(137)}><img src={lista_cadeiras[137].situacao} /></button>
                            <button onClick={()=> chair_selector(138)}><img src={lista_cadeiras[138].situacao} /></button>
                            <button onClick={()=> chair_selector(139)}><img src={lista_cadeiras[139].situacao} /></button>
                            <button onClick={()=> chair_selector(140)}><img src={lista_cadeiras[140].situacao} /></button>
                            <button onClick={()=> chair_selector(141)}><img src={lista_cadeiras[141].situacao} /></button>
                            <button onClick={()=> chair_selector(142)}><img src={lista_cadeiras[142].situacao} /></button> 
                            <button onClick={()=> chair_selector(143)}><img src={lista_cadeiras[143].situacao} /></button>                                                    
                        </div>
                        <div>                        
                            <button onClick={()=> chair_selector(144)}><img src={lista_cadeiras[144].situacao} /></button>
                            <button onClick={()=> chair_selector(145)}><img src={lista_cadeiras[145].situacao} /></button>
                            <button onClick={()=> chair_selector(146)}><img src={lista_cadeiras[146].situacao} /></button>
                            <button onClick={()=> chair_selector(147)}><img src={lista_cadeiras[147].situacao} /></button>
                            <button onClick={()=> chair_selector(148)}><img src={lista_cadeiras[148].situacao} /></button>
                            <button onClick={()=> chair_selector(149)}><img src={lista_cadeiras[149].situacao} /></button>
                            <button onClick={()=> chair_selector(150)}><img src={lista_cadeiras[150].situacao} /></button>
                            <button onClick={()=> chair_selector(151)}><img src={lista_cadeiras[151].situacao} /></button>
                            <button onClick={()=> chair_selector(152)}><img src={lista_cadeiras[152].situacao} /></button>
                            <button onClick={()=> chair_selector(153)}><img src={lista_cadeiras[153].situacao} /></button>
                            <button onClick={()=> chair_selector(154)}><img src={lista_cadeiras[154].situacao} /></button>
                            <button onClick={()=> chair_selector(155)}><img src={lista_cadeiras[155].situacao} /></button>
                            <button onClick={()=> chair_selector(156)}><img src={lista_cadeiras[156].situacao} /></button>
                            <button onClick={()=> chair_selector(157)}><img src={lista_cadeiras[157].situacao} /></button>
                            <button onClick={()=> chair_selector(158)}><img src={lista_cadeiras[158].situacao} /></button>
                            <button onClick={()=> chair_selector(159)}><img src={lista_cadeiras[159].situacao} /></button>
                            <button onClick={()=> chair_selector(160)}><img src={lista_cadeiras[160].situacao} /></button>
                            <button onClick={()=> chair_selector(161)}><img src={lista_cadeiras[161].situacao} /></button>                                                      
                        </div>
                        <div>
                            <button onClick={()=> chair_selector(162)}><img src={lista_cadeiras[162].situacao} /></button>
                            <button onClick={()=> chair_selector(163)}><img src={lista_cadeiras[163].situacao} /></button>
                            <button onClick={()=> chair_selector(164)}><img src={lista_cadeiras[164].situacao} /></button>
                            <button onClick={()=> chair_selector(165)}><img src={lista_cadeiras[165].situacao} /></button>
                            <button onClick={()=> chair_selector(166)}><img src={lista_cadeiras[166].situacao} /></button>
                            <button onClick={()=> chair_selector(167)}><img src={lista_cadeiras[167].situacao} /></button>
                            <button onClick={()=> chair_selector(168)}><img src={lista_cadeiras[168].situacao} /></button>
                            <button onClick={()=> chair_selector(169)}><img src={lista_cadeiras[169].situacao} /></button>
                            <button onClick={()=> chair_selector(170)}><img src={lista_cadeiras[170].situacao} /></button>
                            <button onClick={()=> chair_selector(171)}><img src={lista_cadeiras[171].situacao} /></button>
                            <button onClick={()=> chair_selector(172)}><img src={lista_cadeiras[172].situacao} /></button>
                            <button onClick={()=> chair_selector(173)}><img src={lista_cadeiras[173].situacao} /></button>
                            <button onClick={()=> chair_selector(174)}><img src={lista_cadeiras[174].situacao} /></button>
                            <button onClick={()=> chair_selector(175)}><img src={lista_cadeiras[175].situacao} /></button>
                            <button onClick={()=> chair_selector(176)}><img src={lista_cadeiras[176].situacao} /></button>
                            <button onClick={()=> chair_selector(177)}><img src={lista_cadeiras[177].situacao} /></button>
                            <button onClick={()=> chair_selector(178)}><img src={lista_cadeiras[178].situacao} /></button>
                            <button onClick={()=> chair_selector(179)}><img src={lista_cadeiras[179].situacao} /></button>                           
                        </div>

                    </div>
                    <img src="src/assets/img/A-J.svg" />
                </div>
                <img src="src/assets/img/Legenda.svg" />               
            </aside>
            <div></div> 
            {Modal &&(<Modal isOpen={openModal} onClose={()=>setOpenModal(false)} isOpenNextModal={onNextModal}></Modal>)}  
            {Modal1 &&(<Modal1 isOpen={openModal1} onClose={()=>setOpenModal1(false)}></Modal1>)} 




        </div>
    
        
    )
}