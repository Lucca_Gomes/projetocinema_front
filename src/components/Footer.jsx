import styles from "./footer.module.css"

export function Footer(params) {
    return (
        <footer> 
                {/* Seção Informações */}
            <div className={styles.footer_box}>
                        {/* Seção Endereço */}
                <section className={styles.footer_info}>
                    <div className={styles.footer_address}>
                        <h5>Endereço</h5>
                        <p>Av. Milton Tavares de Souza, <br />
                        s/n - Sala 115 B - Boa Viagem, <br />
                        Niterói - RJ <br />
                        CEP: 24210-315 </p>
                    </div>
                        {/* Seção Contato */}
                    <div className={styles.footer_contact}>
                        <h5>Fale conosco</h5>
                        <a href="mailto:contato@injunior.com.br?subject=Fale conosco">contato@injunior.com.br</a>
                    </div>
                        {/* Seção dos icones  */}
                    <div className={styles.icons}>
                        <a href="https://www.instagram.com/injunioruff/" target="black">
                            <img src="src/assets/img/Icone-Instagram.svg" alt="Icone do Instagram" />
                            </a>
                        <a href="https://www.facebook.com/injunioruff?_rdc=1&_rdr" target="black"> 
                            <img src="src/assets/img/Icone-Facebook.svg" alt="Icone do Facebook" />
                        </a>
                        <a href="https://www.linkedin.com/company/in-junior/" target="black">
                            <img src="src/assets/img/Icone-Linkedin.svg" alt="Icone do Linkedin" />
                        </a>
                    </div>
                </section>
                    {/* Imagem do Ingresso IN Junior */}
                <section>
                    <img src="src/assets/img/INgresso.svg" alt="Imagem ingresso IN" />
                </section>
                    {/* Mapa - Localização IN Junior */}
                <section>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.190302842775!2d-43.13596662385759!3d-22.906350637857056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ed79f10f3%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1692473991277!5m2!1spt-BR!2sbr" width="325" height="244" style={{border: 0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade">
                    </iframe>
                </section>
            </div>   
                    {/* Seção do Copyright  */}
                <section className={styles.copyright}>
                    <p>
                        © Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.
                    </p>
                </section>
        </footer>
    )
}