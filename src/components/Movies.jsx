import { NavLink } from "react-router-dom";
import styles from "./movies.module.css";
import { useState, useEffect } from "react";

export function Movies(params) {
    // const [filmes, setFilmes] = useState(params?.filmes);

    // useEffect(() => {
    //     console.log(filmes);
    // }, []);



    return (
        <main className={styles.main}>
            {
               params?.filmes.map((filme) => ( 
                <div className={styles.main_movie} key={filme.id}>
                    <div className={styles.card_movie}>
                        <img src={filme.imgURL} alt={filme.titulo} />
                    </div>
                    <div className={styles.movie_title}>
                        <h3>{filme.titulo}</h3>
                        <span>{filme.classificacao}</span>
                    </div>
                    <div className={styles.movie_description}>
                        <span>{filme.genero}</span>
                        <span>{filme.diretor}</span>
                        <p>{filme.sinopse}</p>
                    </div>
                    <div className={styles.button_movie}>
                        <NavLink to={`/mostrarsessoes/${filme.id}`}>Ver Sessões</NavLink>
                    </div>
                </div>
            ))}
        </main>
    );
}
