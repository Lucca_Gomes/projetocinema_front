import { NavLink } from 'react-router-dom'
import styles from './header.module.css'

export function Header (params){
    return (
        <div className={styles.header}>
           <NavLink to="/"><img src='src/assets/img/logoINCinema.svg' alt='Logo'/></NavLink> 
            <div>
                <NavLink to="/filmes"><img src='src/assets/img/Movies.svg' alt='Movies'/></NavLink>
                <a href=''><img src='src/assets/img/Signin.svg' alt='Sign in'/></a>
                <NavLink to="/faleconosco"><img src='src/assets/img/Help.svg' alt='Help'/></NavLink>
            </div>
        </div>
    )
}
