import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import styles from "./sessoes.module.css";

export function Sessoes(params) {
    const [activeDimension, setActiveDimension] = useState(null);

    const handleDimensionClick = (dimension) => {
        if (activeDimension === dimension) {
            setActiveDimension(null);  // Clicou novamente, redefina para null
        } else {
            setActiveDimension(dimension);
        }
    };

    return (
        <main className={styles.main}>
            <div>
                <div className={styles.dimension}>
                    <button onClick={() => handleDimensionClick("2d")} className={activeDimension === "2d" ? styles.active : ""}>2d</button>
                    <button onClick={() => handleDimensionClick("3d")} className={activeDimension === "3d" ? styles.active : ""}>3d</button>
                    <button onClick={() => handleDimensionClick("imax")} className={activeDimension === "imax" ? styles.active : ""}>imax</button>
                </div>
                <div>
                    <div className={styles.sessions}>
                        <div className={styles.sessions_box} style={{ display: activeDimension === null || activeDimension === "2d" ? "flex" : "none" }}>
                            <div className={styles.sessions_classification}>
                                <button>2d</button>
                            </div>
                            <div className={styles.sessions_time}>
                                <NavLink to="/checkout">15:20</NavLink>
                                <NavLink to="/checkout">17:40</NavLink>
                                <NavLink to="/checkout">20:00</NavLink>
                                <NavLink to="/checkout">22:10</NavLink>
                            </div>
                        </div>
                        <div className={styles.sessions_box} style={{ display: activeDimension === null || activeDimension === "3d" ? "flex" : "none" }}>
                            <div className={styles.sessions_classification}>
                                <button>3d</button>
                            </div>
                            <div className={styles.sessions_time}>
                                <NavLink to="/checkout">15:30</NavLink>
                                <NavLink to="/checkout">20:15</NavLink>
                            </div>
                        </div>
                        <div className={styles.sessions_box} style={{ display: activeDimension === null || activeDimension === "imax" ? "flex" : "none" }}>
                            <div className={styles.sessions_classification}>
                                <button>imax</button>
                            </div>
                            <div className={styles.sessions_time}>
                                <NavLink to="/checkout">16:20</NavLink>
                                <NavLink to="/checout">18:00</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    );
}
