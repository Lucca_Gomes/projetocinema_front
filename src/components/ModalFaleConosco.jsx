import styles from "./modalfaleconosco.module.css"


export function Modal({ onClose }) {
    return (
        <div className={styles.modal_mensage}>
           <div className={styles.close} onClick={onClose}>
                <img src="src/assets/img/close.svg" alt="" />
            </div>
            <div>
                <h1>Mensagem Enviada!</h1>
                <h5>Obrigado por compartilhar suas observações conosco. </h5>
                <p>Sua contribuição é fundamental para melhorarmos continuamente a <br />sua experiência em nosso site. Valorizamos seu tempo e dedicação <br /> ao relatar esse problema.</p>
            </div>
        </div>
    )
}