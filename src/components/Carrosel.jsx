import { useState } from 'react'
import styles from './carrosel.module.css'

export function Carrosel(params) {

    var [button_list, set_list] = useState (['src/assets/img/ticket.svg','src/assets/img/ticket2.svg','src/assets/img/ticket3.svg']);

    function handleLeftClick (){
        set_list([button_list[2],button_list[0],button_list[1]]);
    }
    
    function handleRightClick (){
        set_list([button_list[1],button_list[2],button_list[0]]);
    }

    return (
    <div className={styles.body}>
        <div>
            <div>
                <button onClick={handleLeftClick}><img src='src/assets/img/button_left.svg' alt='Button left'/></button>
                <button onClick={handleRightClick}><img src='src/assets/img/button_right.svg' alt='Button right'/></button>
            </div>
            <img src={button_list[0]} alt='Ticket'/>             
        </div>           
    </div>
    )
}