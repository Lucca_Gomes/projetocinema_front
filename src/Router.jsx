// import "./global.css"
import { BaseLayout } from "./assets/layouts/BaseLayout";
import { Filmes } from "./pages/Filmes";
import {Routes, Route} from "react-router-dom";
import { HomePage } from "./pages/HomePage";
import { MostrarSessoes } from "./pages/MostrarSessoes";
import { FaleConosco } from "./pages/FaleConosco";
import { Checkout } from "./pages/Checkout";


export function Router(params) {
    return ( 
    <Routes>
        <Route path="/" element={<BaseLayout/>}>
            <Route path="/" element={<HomePage/>}/>
            <Route path="filmes" element={<Filmes/>}/>
            <Route path="mostrarsessoes" element={<MostrarSessoes/>}/>
            <Route path="faleconosco" element={<FaleConosco/>}/>
            <Route path="checkout" element={<Checkout/>}/>
            <Route path="mostrarsessoes/:filmeId" element={<MostrarSessoes/>}/>
            <Route path="faleconosco" element={<FaleConosco/>}/>
        </Route>    
    </Routes>
)
}

